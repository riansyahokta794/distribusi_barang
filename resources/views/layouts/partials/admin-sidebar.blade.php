<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><small>PT. Sinar Laut Cemerlang</small> </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    @if (Auth::user()->role == 'admin')
    <li class="nav-item {{ Request::is('*admin*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ url('/admin/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Manajemen Barang
    </div>


    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item  {{ Request::is('admin/barang*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#dataBarang" aria-expanded="true"
            aria-controls="dataBarang">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <span>Kelola Barang</span>
        </a>
        <div id="dataBarang" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Data Barang:</h6>
                <a class="collapse-item" href="{{ route('barang.show') }}">Lihat Data Barang</a>
            </div>
        </div>
    </li>

    <li class="nav-item  {{ Request::is('/admin/pesanan*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#dataPesanan" aria-expanded="true"
            aria-controls="dataPesanan">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <span>Kelola Pesanan</span>
        </a>
        <div id="dataPesanan" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Data Pemesanan:</h6>
                <a class="collapse-item" href="{{ route('show.pesanan.pelanggan') }}">Data Pemesanan</a>
            </div>
        </div>
    </li>

    <li class="nav-item  {{ Request::is('/admin/pengiriman*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#dataPengiriman" aria-expanded="true"
            aria-controls="dataPengiriman">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <span>Kelola Pengiriman</span>
        </a>
        <div id="dataPengiriman" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Data Pemesanan:</h6>
                <a class="collapse-item" href="{{ route('show.pengiriman') }}">Data Pengiriman</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        MANAJEMEN PENGGUNA
    </div>
    <!-- Nav Item - Charts -->
    {{-- <li class="nav-item {{ Request::is('admin/pelanggan*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ route('pelanggan.show') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Konsumen</span></a>
    </li> --}}
    <li class="nav-item {{ Request::is('admin/supplier*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ url('admin/supplier') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Supplier</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('user.show') }}">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Users</span></a>
    </li>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        MANAJEMEN TRANSPORTASI
    </div>
    <!-- Nav Item - Charts -->
    <li class="nav-item {{ Request::is('admin/supir*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ route('show.transportasi') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Data Transportasi</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    @elseif(Auth::user()->role == 'purchases')
    <li class="nav-item {{ Request::is('*home*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ url('/home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <li class="nav-item  {{ Request::is('pesanan*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <span>Kelola Pesanan</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Data Pemesanan:</h6>
                <a class="collapse-item" href="{{ route('show.pesanan') }}">Data Pemesanan</a>
            </div>
        </div>
    </li>
    @else
    <li class="nav-item {{ Request::is('*home*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ url('/home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item {{ Request::is('produk*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ route('show.produk') }}">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <span>List Produk   </span></a>
    </li>
    <li class="nav-item  {{ Request::is('pesanan*') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <span>Kelola Pesanan</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Data Pemesanan:</h6>
                <a class="collapse-item" href="{{ route('show.pesanan') }}">Data Pemesanan</a>
            </div>
        </div>
    </li>
    <li class="nav-item {{ Request::is('*home*') ? 'active' : ''  }}">
        <a class="nav-link" href="{{ url('/home') }}">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Pengaturan Akun</span></a>
    </li>
    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>