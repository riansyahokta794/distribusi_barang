@extends('layouts.main')
@section('title', 'Pesanan')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Detail Pesanan</h1>
                <a href="{{ route('show.pesanan') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="myDataTable" class="table table-bordered m-0 w-100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total</th>
                            @if ($pesanan != null && $pesanan->status == 'Menunggu')
                            <th>Aksi</th>
                            @else
                                
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$detailPesanan->isEmpty())
                        @foreach ($detailPesanan as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{ $item->barangs->nama }}
                            </td>
                            <td>{{ $item->jumlah }}</td>
                            <td>Rp. {{ number_format($item->harga, 0, ',', '.') }}</td>
                            <td>Rp. {{ number_format($item->subtotal, 0, ',', '.') }}</td>
                            @if ($pesanan->status == 'Menunggu')    
                            <td>
                                <a href="{{ route('edit.pesanan', $item->id) }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i></a>
                                <form id="hapus-data-{{ $item->id }}" action="{{ route('hapus.pesanan', $item->id) }}" method="POST" class="d-none">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-remove"><i class="icon-close"></i></button>
                                </form>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('hapus-data-{{ $item->id }}').submit();" class="btn btn-danger"><i class="bi bi-trash3"></i></a>
                            </td>
                            @else
                                
                            @endif
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">Belum Ada Data Yang Ditambahkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <div class="row d-flex justify-content-between align-items-center">
                <div class="col-lg-4">
                    <ul>
                        <li>Jumlah Pesanan : {{ $detailPesanan->count() }}</li>
                        <li>Total Biaya : Rp. {{ number_format($totalBiaya, 0, ',', '.') }}</li>
                        @isset($penawaran)
                        <li>
                            <a href=""></a>
                        </li>
                        @endisset
                    </ul>
                </div>
                @if ($pesanan->status == 'Sedang Diproses')
                {{-- <a href="{{ route('kirim.permintaan', $pesanan->id) }}" class="btn btn-primary mx-3">Konfirmasi Pesanan</a> --}}
                <div class="col-lg-4 ">
                    <div class="card bg-secondary text-white shadow">
                        <div class="p-3 card-body text-center">
                            Pesanan Sedang Diproses
                        </div>
                    </div>
                </div>
                @elseif($pesanan->status == 'Dikirim')
                <div class="col-lg-4 ">
                    <div class="card bg-primary text-white shadow">
                        <div class="p-3 card-body text-center">
                            Pesanan Sedang Dikirim
                        </div>
                    </div>
                </div>
                @else
                <a href="{{ route('kirim.permintaan', $pesanan->id) }}" class="btn btn-primary mx-3">Proses Pesanan</a>
                @endif
            </div>

        </div>
    </div>
    @if ($pengiriman != null )     
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-primary">Detail Pengiriman</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseCardExample" style="">
            <div class="card-body">
                <div class="mb-3">
                    <label for="no_pesanan" class="form-label">No Pesanan : *</label>
                    <input type="text" name="" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                        value="{{ $pesanan->no_pesanan }}" disabled>
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">No Pesanan : *</label>
                    <input type="text" name="" id="alamat" class="form-control @error('alamat') is-invalid @enderror"
                        value="{{ $pengiriman->alamat }}" disabled>
                </div>
                <div class="mb-3">
                    <label for="transportasi_id" class="form-label">Transportasi : *</label>
                    <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                        value="{{ $pengiriman->transportasi->no_plat }}" disabled >
                </div>
                <div class="mb-3">
                    <label for="user_id" class="form-label">Supir : *</label>
                    <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                    value="{{ $pengiriman->user->name }}" disabled>
                </div>
                <div class="mb-3">
                    <label for="user_id" class="form-label">Tanggal Pengiriman : *</label>
                    <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                    value="{{ $pengiriman->tanggal }}" disabled>
                </div>
                <div class="mb-3">
                    <label for="user_id" class="form-label">Status Pengiriman : *</label>
                    <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                    value="{{ $pengiriman->status }}" disabled>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection