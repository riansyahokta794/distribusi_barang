@extends('layouts.main')
@section('title', 'Pesanan')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Daftar Pesanan</h1>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="myDataTable" class="table table-bordered m-0 w-100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>No Pesanan</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$pesanans->isEmpty())
                        @foreach ($pesanans as $pesanan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $pesanan->no_pesanan }}</td>
                            <td>{{ $pesanan->tanggal }}</td>
                            @if ($pesanan->status == 'Menunggu')
                                
                            @else
                                
                            @endif
                            <td><span class="mt-5 alert @if ($pesanan->status == 'Menunggu') alert-warning @elseif($pesanan->status == 'Sedang Diproses') alert-secondary @elseif($pesanan->status == 'Dikirim') alert-primary @elseif($pesanan->status == 'Diterima') alert-success @endif "> {{ $pesanan->status }} </span></td>
                            <td>
                                <a href="{{ route('detail.pesanan', $pesanan->id) }}" class="btn btn-primary"><i class="bi bi-eye"></i> Lihat</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">Belum Ada Data Yang Ditambahkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection