@extends('layouts.main')
@section('title', 'Pesan Barang')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Pesan Barang</h1>
                <a href="{{ route('show.produk') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('simpan.pesanan', $barang->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama Barang : *</label>
                    <input type="text" name="nama" id="nama" class="form-control @error('nama') is-invalid @enderror" disabled
                        value="{{ $barang->nama }}">
                </div>
                <div class="mb-3">
                    <label for="harga" class="form-label">Harga Satuan: *</label>
                    <input type="number" name="harga" id="harga" disabled
                        class="form-control @error('harga') is-invalid @enderror" value="{{ $barang->harga }}">
                </div>
                <div class="mb-3">
                    <label for="jumlah" class="form-label">Jumlah : *</label>
                    <input type="number" name="jumlah" id="jumlah"
                        class="form-control @error('jumlah') is-invalid @enderror" value="{{ old('jumlah') }}">
                    @error('jumlah')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="mb-3">
                    <button id="tambahPesanan" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
   
</script>    
@endsection