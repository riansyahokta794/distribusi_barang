@extends('layouts.main')
@section('title', 'Data Produk')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Data Produk</h1>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="myDataTable" class="table table-bordered m-0 w-100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Produk</th>
                            <th>Deskripsi</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$barangs->isEmpty())
                        @foreach ($barangs as $barang)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $barang->nama }}</td>
                            <td>{{ $barang->deskripsi }}</td>
                            <td>{{ "Rp. ".number_format($barang->harga,0,',','.') }} </td>
                            <td>
                                <a href="{{ route('tambah.pesanan', $barang->slug) }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i> Pesan</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">Belum Ada Data Yang Dapat Ditampilkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection