@extends('layouts.main')
@section('title', 'Supplier')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Data Supplier</h1>
                <a href="{{ route('supplier.tambah') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-plus-circle"></i> Tambah Supplier
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table @if($suppliers->isNotEmpty()) id="myDataTable" @endif class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Supplier</th>
                            <th>Alamat</th>
                            <th>No Whatsapp / Telepon</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$suppliers->isEmpty())
                        @foreach ($suppliers as $supplier)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $supplier->nama }}</td>
                            <td>{{ $supplier->alamat }}</td>
                            <td>{{ $supplier->no_telepon }}</td>
                            <td>
                                <a href="{{ route('supplier.edit', $supplier->id) }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i></a>
                                <form id="hapus-data-{{ $supplier->id }}" action="{{ route('supplier.hapus', $supplier->id) }}" method="POST" class="d-none">
                                    @csrf
                                    @method('DELETE')
                                    {{-- <button type="submit" class="btn-remove"><i class="icon-close"></i></button> --}}
                                </form>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('hapus-data-{{ $supplier->id }}').submit();" class="btn btn-danger"><i  class="bi bi-trash3"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">Belum Ada Data Yang Ditambahkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection