@extends('layouts.main')
@section('title', 'Kategori')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Edit Data Barang</h1>
                <a href="{{ route('barang.show') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('barang.update', $data->id) }}" method="post">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama Barang : *</label>
                    <input type="text" name="nama" id="nama" class="form-control @error('nama') is-invalid @enderror"
                        value="{{ $data->nama }}">
                    @error('nama')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="kd_stok" class="form-label">Kode Stok : *</label>
                    <input type="text" name="kd_stok" id="kd_stok"
                        class="form-control @error('kd_stok') is-invalid @enderror" value="{{ $data->kd_stok }}">
                    @error('kd_stok')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="deskripsi" class="form-label">Deskripsi Barang : *</label>
                    <input type="text" name="deskripsi" id="deskripsi"
                        class="form-control @error('deskripsi') is-invalid @enderror" value="{{ $data->deskripsi }}">
                    @error('deskripsi')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="harga" class="form-label">Harga : *</label>
                    <input type="number" name="harga" id="harga"
                        class="form-control @error('harga') is-invalid @enderror" value="{{ $data->harga }}">
                        <small class="form-hint">
                            Hindari penggunaan simbol titik atau koma, input angka biasa ! contoh : 100000
                        </small>
                    @error('harga')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="stok" class="form-label">Stok : *</label>
                    <input type="number" name="stok" id="stok"
                        class="form-control @error('stok') is-invalid @enderror" value="{{ $data->stok }}">
                    @error('stok')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection