@extends('layouts.main')
@section('title', 'Transportasi')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Tambah Data Transportasi</h1>
                <a href="{{ route('show.transportasi') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ url('/admin/transportasi/tambah') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="no_plat" class="form-label">No Plat Kendaraan : *</label>
                    <input type="text" name="no_plat" id="no_plat" class="form-control @error('no_plat') is-invalid @enderror"
                        value="{{ old('no_plat') }}">
                    @error('no_plat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="jenis" class="form-label">Jenis Kendaraan : *</label>
                    <input type="text" name="jenis" id="jenis"
                        class="form-control @error('jenis') is-invalid @enderror" value="{{ old('jenis') }}">
                    @error('jenis')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="tahun" class="form-label">Tahun Kendaraan : *</label>
                    <input type="number" name="tahun" id="tahun"
                        class="form-control @error('tahun') is-invalid @enderror" value="{{ old('tahun') }}">
                    @error('tahun')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection