@extends('layouts.main')
@section('title', 'User')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Data User</h1>
                <a href="{{ route('user.tambah') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-plus-circle"></i> Tambah User
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table @if($users->isNotEmpty()) id="myDataTable" @endif class="table table-bordered m-0 w-100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama User</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$users->isEmpty())
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role }}</td>
                            <td>
                                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i></a>
                                <form id="hapus-data-{{ $user->id }}" action="{{ route('user.hapus', $user->id) }}" method="POST" class="d-none">
                                    @csrf
                                    @method('DELETE')
                                    {{-- <button type="submit" class="btn-remove"><i class="icon-close"></i></button> --}}
                                </form>
                                <a href="#" onclick="event.preventDefault(); document.getElementById('hapus-data-{{ $user->id }}').submit();" class="btn btn-danger"><i  class="bi bi-trash3"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">Belum Ada Data Yang Ditambahkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection