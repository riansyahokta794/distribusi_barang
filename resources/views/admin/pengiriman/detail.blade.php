@extends('layouts.main')
@section('title', 'Detail Pengiriman')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Detail Pengiriman Barang</h1>
                <a href="{{ route('show.pengiriman') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <label for="no_pesanan" class="form-label">No Pesanan : *</label>
                <input type="text" name="" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                    value="{{ $pengiriman->pesanan->no_pesanan }}" disabled>
                @error('no_pesanan')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="alamat" class="form-label">Alamat Pengiriman : *</label>
                <input type="text" name="" id="alamat" class="form-control @error('alamat') is-invalid @enderror"
                    value="{{ $pengiriman->alamat }}" disabled>
                @error('alamat')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="transportasi_id" class="form-label">Transportasi : *</label>
                <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                    value="{{ $pengiriman->transportasi->no_plat }}" disabled >
            </div>
            <div class="mb-3">
                <label for="user_id" class="form-label">Supir : *</label>
                <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                value="{{ $pengiriman->user->name }}" disabled>
            </div>
            <div class="mb-3">
                <label for="user_id" class="form-label">Tanggal Pengiriman : *</label>
                <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                value="{{ $pengiriman->tanggal }}" disabled>
            </div>
            <div class="mb-3">
                <label for="user_id" class="form-label">Status Pengiriman : *</label>
                <input type="text" name="no_pesanan" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                value="{{ $pengiriman->status }}" disabled>
            </div>
        </div>
    </div>
</div>
@endsection