@extends('layouts.main')
@section('title', 'Pengiriman')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Atur Pengiriman Barang</h1>
                <a href="{{ route('show.pesanan.pelanggan') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('simpan.pengiriman.pelanggan') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="no_pesanan" class="form-label">No Pesanan : *</label>
                    <input type="text" name="" id="no_pesanan" class="form-control @error('no_pesanan') is-invalid @enderror"
                        value="{{ $pesanan->no_pesanan }}" disabled>
                    <input type="text" name="no_pesanan" id="no_pesanan" class="d-none form-control @error('no_pesanan') is-invalid @enderror"
                        value="{{ $pesanan->id }}" >
                    @error('no_pesanan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat Tujuan : *</label>
                    <input type="text" name="alamat" id="alamat" class="form-control @error('alamat') is-invalid @enderror"
                        value="{{ $pesanan->user->alamat }}" disabled>
                    @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="transportasi_id" class="form-label">Transportasi : *</label>
                    <select class="form-control @error('transportasi_id') is-invalid @enderror" name="transportasi_id" value="{{ old('transportasi_id') }}">
                        <option value="">-- Pilih Kendaraan --</option>
                        @foreach ($transportasi as $item)
                        <option value="{{ $item->id }}" {{ old('transportasi_id') == $item->id ? 'selected' : '' }}>{{ $item->no_plat }}</option>
                        @endforeach
                    </select>
                    @error('transportasi_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="user_id" class="form-label">Supir : *</label>
                    <select class="form-control @error('user_id') is-invalid @enderror" name="user_id" value="{{ old('user_id') }}">
                        <option value="">-- Pilih Supir --</option>
                        @if ($supir->isNotEmpty() && $supir != null)
                            @foreach ($supir as $item)
                            <option value="{{ $item->id }}" {{ old('user_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                            @endforeach
                        @else
                            <option value="">Belum Ada Supir Yang Ditambahkan </option>
                        @endif
                    </select>
                    @error('user_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection