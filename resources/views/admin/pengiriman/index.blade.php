@extends('layouts.main')
@section('title', 'Pengiriman')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Data Pengiriman</h1>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table @if($pengirimans->isNotEmpty()) id="myDataTable" @endif class="table table-bordered m-0 w-100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>No. Pesanan</th>
                            <th>Nama Supir</th>
                            <th>No Plat Kendaraan</th>
                            <th>Tanggal pengiriman</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$pengirimans->isEmpty())
                        @foreach ($pengirimans as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->pesanan->no_pesanan }}</td>
                            <td>{{ $item->user->name }}</td>
                            <td>{{ $item->transportasi->no_plat }}</td>
                            <td>{{ $item->tanggal }}</td>
                            <td>
                                <span class="mt-5 alert @if ($item->status == 'Sedang Dikirim') alert-warning @else alert-success @endif ">
                                    {{ $item->status }}
                                </span>
                            </td>
                            <td>
                                <a href="{{ route('show.detailpengiriman.pelanggan', $item->pesanan->id) }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i> Lihat</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">Belum Ada Data Yang Ditambahkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection