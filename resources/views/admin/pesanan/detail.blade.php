@extends('layouts.main')
@section('title', 'Pesanan')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Detail Pesanan</h1>
                <a href="{{ route('show.pesanan.pelanggan') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-arrow-left-circle"></i> Kembali
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$detailPesanan->isEmpty())
                        @foreach ($detailPesanan as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{ $item->barangs->nama }}
                            </td>
                            <td>{{ $item->jumlah }}</td>
                            <td>Rp. {{ number_format($item->harga, 0, ',', '.') }}</td>
                            <td>Rp. {{ number_format($item->subtotal, 0, ',', '.') }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7" class="text-center">Belum Ada Data Yang Ditambahkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <div class="row d-flex justify-content-between align-items-center">
                <div class="col-lg-4">
                    <ul>
                        <li>Jumlah Pesanan : {{ $detailPesanan->count() }}</li>
                        <li>Total Biaya : Rp. {{ number_format($totalBiaya, 0, ',', '.') }}</li>
                    </ul>
                </div>
                @if ($pesanan->status == 'Menunggu')
                    <a href="{{ route('terima.pesanan.pelanggan', $pesanan->id) }}" class="btn btn-primary mx-3">Terima Pesanan</a>
                @elseif($pesanan->status == 'Sedang Diproses')
                    {{-- <a href="{{ route('kirim.permintaan', $pesanan->id) }}" class="btn btn-primary mx-3">Konfirmasi Pesanan</a> --}}
                    <a href="{{ route('tambah.pengiriman.pelanggan', $pesanan->id) }}" class="btn btn-primary mx-3">Atur Pengiriman</a>
                @elseif($pesanan->status == 'Dikirim')
                    {{-- <a href="{{ route('kirim.permintaan', $pesanan->id) }}" class="btn btn-primary mx-3">Konfirmasi Pesanan</a> --}}
                    <a href="{{ route('show.detailpengiriman.pelanggan', $pesanan->id) }}" class="btn btn-primary mx-3">Lihat Detail Pengiriman</a>
                @endif
            </div>

        </div>
    </div>
</div>
@endsection