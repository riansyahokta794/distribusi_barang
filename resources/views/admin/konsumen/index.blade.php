@extends('layouts.main')
@section('title', 'Konsumen')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="d-sm-flex align-items-center justify-content-between">
                <h1 class="h3 mb-0 text-gray-800">Data Konsumen</h1>
                <a href="{{ route('konsumen.tambah') }}" class="d-none d-sm-inline-block btn btn-primary shadow-sm">
                    <i class="bi bi-plus-circle"></i> Tambah Konsumen
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Konsumen</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th>No Whatsapp / Telepon</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!$konsumens->isEmpty())
                        @foreach ($konsumens as $konsumen)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $konsumen->nama }}</td>
                            <td>{{ $konsumen->email }}</td>
                            <td>{{ $konsumen->alamat }}</td>
                            <td>{{ $konsumen->no_telepon }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6" class="text-center">Belum Ada Data Yang Dapat Ditampilkan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection