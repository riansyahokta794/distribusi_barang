<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Pesanan;
use App\Models\Pengiriman;
use Illuminate\Http\Request;
use App\Models\DetailPesanan;
use App\Http\Controllers\Controller;

class PesananController extends Controller
{
    public function showProduk(){
        $barangs = Barang::all();
        $title = 'Hapus Data!';
        $text = "Data Konsumen Akan Dihapus?";
        confirmDelete($title, $text);
        return view('main.produk.index', compact('barangs'));
    }
    public function addPesanan($slug){
        $barang = Barang::where('slug',$slug)->first();
        // dd($barang);
        return view('main.pesanan.create', compact('barang'));
    }
    public function storePesanan(Request $request, $id_barang){
        $barang = Barang::findOrFail($id_barang);
        $request->validate([
            'jumlah' => 'required|numeric'
        ]);

        $pesanan = Pesanan::updateOrCreate([
            'user_id' => Auth()->user()->id,
            'status' => 'Menunggu'
        ],[
            'no_pesanan' => 'ORD' . date('YmdHis'),
            'tanggal' => now(),
        ]);
        DetailPesanan::updateOrCreate([
            'pesanan_id' => $pesanan->id,
            'barang_id' => $id_barang,
        ],[
            'jumlah' => $request->jumlah,
            'harga' => $barang->harga,
            'subtotal' => $barang->harga * $request->jumlah,
        ]);
        return redirect('/produk')->with('status', 'Pesanan Berhasl Ditambahkan');
        // dd($detailPesanan);
    }
    public function showPesanan(){
        $pesanans = Pesanan::where('user_id', Auth()->user()->id)->get();
        // $detailPesanans = DetailPesanan::where('pesanan_id', $pesanan->id)->get();
        // dd($detailPesanans); 
        return view('main.pesanan.index', compact('pesanans'));
    }
    public function showDetailPesanan($id){
        $pesanan = Pesanan::with('detailPesanan')->where('id', $id)->first();
        $pengiriman = Pengiriman::where('no_pesanan', $id)->with('user')->with('transportasi')->first(); 
        // dd($pengiriman);
        $detailPesanan = DetailPesanan::with('barangs')->where('pesanan_id', $id)->get();
        // $pesanan = Pesanan::where('id', $id)->first();
        // dd($pesanan);
        $totalBiaya = 0 ;
        foreach($detailPesanan as $item){
            $totalBiaya += $item->subtotal;
        }
        // dd($totalBiaya);
        return view('main.pesanan.detail', compact('detailPesanan', 'pesanan', 'totalBiaya', 'pengiriman'));
    }
    public function editPesanan($slug){
        $pesanan = DetailPesanan::with('barangs')->findOrFail($slug);
        return view('main.pesanan.edit', compact('pesanan'));
    }
    public function updatePesanan(Request $request, $id){
        $request->validate([
            'jumlah' => 'required|numeric'
        ]);

        $pesanan = DetailPesanan::findOrFail($id);
        $pesanan->update([
            'jumlah' => $request->jumlah,
            'subtotal' => $pesanan->harga * $request->jumlah
        ]);
        // dd($pesanan);
        return redirect('/pesanan')->with('status', 'Data Pesanan Berhasil Diubah !');
    }
    public function prosesPesanan($id){
        $pesanan = Pesanan::findOrFail($id);
        // $pesanan->status = 'Sedang Diproses';
        // $pesanan->save();
        alert()->success('Hore!', 'Permintaan Telah Dikirim dan Akan Diproses Oleh Admin');
        return redirect('/pesanan');
        // dd($pesanan);
    }
    public function destroy($id){
        DetailPesanan::findOrFail($id)->delete();
        $pesanan = Pesanan::with('detailPesanan')->where('user_id', Auth()->user()->id)->where('status', 'Menunggu')->first();
        // dd($pesanan);
        if($pesanan->detailPesanan->count() < 1){
            $pesanan->delete();
        }
        alert()->success('Hore!','Data Berhasil Dihapus');
        return redirect('pesanan');
    }
}
