<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index(){
        $suppliers = Supplier::all();
        return view('admin.supplier.index', compact('suppliers'));
    }
    public function create(){
        return view('admin.supplier.create');
    }

    public function store(Request $request){
        $data = $request->validate([
            'nama' => 'required|string|min:5|max:255',
            'alamat' => 'required|string|min:5|max:255',
            'no_telepon' => 'required|string|min:10|max:255',
        ]);

        Supplier::create($data);
        return redirect('admin/supplier')->with('status', 'Data Konsumen Berhasil Ditambahkan');
    }

    public function edit($id){
        $data = Supplier::findOrFail($id);
        return view('admin.supplier.edit', compact('data'));
    }
    public function update(Request $request, $id){
        $data = $request->validate([
            'nama' => 'required|string|min:5|max:255',
            'alamat' => 'required|string|min:5|max:255',
            'no_telepon' => 'required|string|min:10|max:255',
        ]);
        Supplier::findOrFail($id)->update($data);
        return redirect('admin/supplier')->with('status', 'Data Konsumen Berhasil Di Ubah!');
    }
    public function destroy($id){
        Supplier::findOrFail($id)->delete();
        alert()->success('Hore!','Data Berhasil Dihapus');
        return back();
    }
}
