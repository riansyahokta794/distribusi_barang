<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use RegistersUsers;
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(){
        $users = User::whereNotIn('role', ['user', 'admin'])->get();
        // dd($users);
        return view('admin.user.index', compact('users'));
    }
    public function create(){
        return view('admin.user.create');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect('/admin/user')->with('status', 'Data User Berhasil Ditambahkan !');
    }
    public function edit($id){
        $user = User::findOrFail($id);
        return view('admin.user.edit', compact('user'));
    }
    public function update(Request $request, $id){
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        User::findOrFail($id)->update([
            'email' => $data['email'],
        ],[
            'name' => $data['name'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect('admin/user')->with('status', 'Data User Berhasil Di Ubah !');
    }
    public function destroy($id){
        User::findOrFail($id)->delete();
        alert()->success('Hore!','Data Berhasil Dihapus');
        return back();
    }
}
