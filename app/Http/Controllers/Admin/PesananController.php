<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pesanan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DetailPesanan;

class PesananController extends Controller
{
    public function index(){
        $pesanans = Pesanan::with('user')->get();
        // dd($pesanans);
        return view('admin.pesanan.index', compact('pesanans'));
    }

    public function showDetailPesanan($id){
        $pesanan = Pesanan::with('detailPesanan')->where('id', $id)->first();
        // dd($pesanan);
        $detailPesanan = DetailPesanan::with('barangs')->where('pesanan_id', $id)->get();
        $totalBiaya = 0 ;
        foreach($detailPesanan as $item){
            $totalBiaya += $item->subtotal;
        }
        return view('admin.pesanan.detail', compact('pesanan', 'detailPesanan', 'totalBiaya'));
    }
    public function confirmPesanan($id){
        $pesanan = Pesanan::where('id', $id)->first();
        
        $pesanan->status = 'Sedang Diproses';
        $pesanan->save();
        alert()->info('Info!', 'Pesanan Diterima, Harap Siapkan Barang Pesanan Untuk Dikirim');
        return redirect('admin/pesanan');
    }
    
}

