<?php

namespace App\Http\Controllers\Admin;

use App\Models\Barang;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarangController extends Controller
{
    public function index(){
        $barangs = Barang::all();
        $title = 'Hapus Data!';
        $text = "Data Konsumen Akan Dihapus?";
        confirmDelete($title, $text);
        return view('admin.barang.index',compact('barangs'));
    }
    public function create(){
        return view('admin.barang.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|string|min:5|max:255',
            'kd_stok' => 'required|string|min:5|max:255',
            'deskripsi' => 'required|string|min:5|max:255',
            'harga' => 'required|integer|min:0',
            'stok' => 'required|integer|min:0',
        ]);
        Barang::create([
            'nama' => $request->nama,
            'slug' => Str::slug($request->nama),
            'kd_stok' => $request->kd_stok,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
            'stok' => $request->stok
        ]);
        return redirect('admin/barang')->with('status', 'Data Berhasil Ditambahkan!');
    }
    public function edit($id){
        $data = Barang::findOrFail($id);
        return view('admin.barang.edit', compact('data'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required|string|min:5|max:255',
            'kd_stok' => 'required|string|min:5|max:255',
            'deskripsi' => 'required|string|min:5|max:255',
            'harga' => 'required|integer|min:0',
            'stok' => 'required|integer|min:0',
        ]);
        Barang::findOrFail($id)->update([
            'nama' => $request->nama,
            'slug' => Str::slug($request->nama),
            'kd_stok' => $request->kd_stok,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,
            'stok' => $request->stok
        ]);
        return redirect('/admin/barang')->with('status', 'Data Barang Berhasil Diubah');
    }
    public function destroy($id) {
        Barang::findOrFail($id)->delete();
        alert()->success('Hore!','Data Berhasil Dihapus');
        return back();
    }
}
