<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transportasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransportasiController extends Controller
{
    public function index(){
        $transportasis = Transportasi::all();
        return view('admin.transportasi.index', compact('transportasis'));
    }
    public function create(){
        return view('admin.transportasi.create');
    }
    public function store(Request $request){
        $request->validate([
            'no_plat' => 'required|string|min:7',
            'jenis' => 'required|string|min:4',
            'tahun' => 'required|string|min:4|max:4',
        ]);

        Transportasi::create($request->all());
        return redirect('admin/transportasi')->with('status', 'Data Berhasil Ditambahkan!');
    }
    public function edit($id){
        $transportasi = Transportasi::findOrFail($id);
        return view('admin.transportasi.edit',compact('transportasi'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'no_plat' => 'required|string|min:7',
            'jenis' => 'required|string|min:4',
            'tahun' => 'required|string|min:4|max:4',
        ]);
        Transportasi::findOrFail($id)->update($request->all());
        return redirect('admin/transportasi')->with('status', 'Data Berhasil Diubah!');
    }
    public function destroy($id){
        Transportasi::findOrFail($id)->delete();
        alert()->success('Hore!','Data Berhasil Dihapus');
        return back();
    }
    
}
