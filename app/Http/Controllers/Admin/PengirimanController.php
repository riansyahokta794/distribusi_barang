<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Pesanan;
use App\Models\Pengiriman;
use App\Models\Transportasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\Mailer\Transport;

class PengirimanController extends Controller
{
    public function index(){
        $pengirimans = Pengiriman::with('transportasi')->with('user')->with('pesanan')->get();
        // dd($pengirimans);
        return view('admin.pengiriman.index', compact('pengirimans'));
    }
    public function prosesPesanan($id){
        $pesanan = Pesanan::findOrFail($id);
        $pesanan->status = 'Sedang Diproses';
        $pesanan->save();
        alert()->success('Hore!', 'Harap Segera Mempersiapkan Barang Untuk Dilakukan Pengiriman');
        return redirect('admin/pesanan');
        // dd($pesanan);
    }
    public function create($id){
        $pesanan = Pesanan::findOrFail($id)->with('user')->first();
        $supir = User::where('role', 'Logistik')->get();
        $transportasi = Transportasi::all();
        // dd($pesanan);
        return view('admin.pengiriman.create', compact('pesanan', 'supir', 'transportasi'));
    }
    public function store(Request $request){
        // dd($request);
        $request->validate([
            'no_pesanan' => 'required',
            'transportasi_id' => 'required',
            'user_id' => 'required',
        ]);
        $pesanan = Pesanan::where('id',$request->no_pesanan)->with('user')->first();
        // dd($pesanan);
        $pesanan->status = 'Dikirim';
        $pesanan->save();
        // dd($pesanan);
        Pengiriman::create([
            'no_pesanan' => $request->no_pesanan,
            'transportasi_id' => $request->transportasi_id,
            'user_id' => $request->user_id,
            'tanggal' => now(),
            'alamat' => $pesanan->user->alamat,
            'status' => 'Sedang Dikirim'
        ]);
        return redirect('admin/pengiriman')->with('status', 'Data Pengiriman Berhasil Ditambahkan!');
    }
    public function showDetail($id){
        
        $pengiriman = Pengiriman::where('no_pesanan',$id)->with('transportasi')->with('user')->with('pesanan')->first();
        // dd($pengiriman);
        return view('admin.pengiriman.detail', compact('pengiriman'));
    }
}
