<?php

namespace App\Http\Controllers\Admin;

use App\Models\Konsumen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PelangganController extends Controller
{
    public function index()
    {
        $konsumens = Konsumen::all();
        $title = 'Hapus Data!';
        $text = "Data Konsumen Akan Dihapus?";
        confirmDelete($title, $text);
        return view('admin.konsumen.index', compact('konsumens'));
    }
    public function create()
    {
        return view('admin.konsumen.create');
    }
    public function store(Request $request)
    {
        $data = $request->validate([
            'nama' => 'required|string|min:5|max:255',
            'email' => 'required|email|min:5|max:255',
            'alamat' => 'required|string|min:5|max:255',
            'no_telepon' => 'required|string|min:10|max:255',
        ]);

        Konsumen::create($data);
        return redirect('admin/konsumen')->with('status', 'Data Konsumen Berhasil Ditambahkan');
    }
    public function edit($id)
    {
        $data = Konsumen::firstWhere('id', $id);
        return view('admin.konsumen.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nama' => 'required|string|min:5|max:255',
            'email' => 'required|email|min:5|max:255',
            'alamat' => 'required|string|min:5|max:255',
            'no_telepon' => 'required|string|min:10|max:255',
        ]);

        Konsumen::findOrFail($id)->update($data);
        return redirect('admin/konsumen')->with('status', 'Data Konsumen Berhasil Diubah!');
    }
    public function destroy($id) {
        Konsumen::findOrFail($id)->delete();
        alert()->success('Hore!','Data Berhasil Dihapus');
        return back();
    }
}
