<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    use HasFactory;
    protected $table = 'pengirimans';
    protected $fillable = [
        'no_pesanan',
        'transportasi_id',
        'user_id',
        'tanggal',
        'alamat',
        'status',
    ];
    public function pesanan(){
        return $this->belongsTo(Pesanan::class, 'no_pesanan', 'id');
    }
    public function transportasi(){
        return $this->belongsTo(Transportasi::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
