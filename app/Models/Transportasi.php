<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportasi extends Model
{
    use HasFactory;
    protected $table = 'transportasis';
    protected $fillable = [
        'no_plat',
        'jenis',
        'tahun',
    ];
}
