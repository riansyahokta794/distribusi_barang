<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPesanan extends Model
{
    use HasFactory;

    protected $table = 'detail_pesanans';
    protected $fillable = [
        'pesanan_id',
        'barang_id',
        'jumlah',
        'harga',
        'subtotal',
    ];
    public function pesanan(){
        return $this->belongsTo(Pesanan::class, 'pesanan_id', 'id');
    }
    public function barangs(){
        return $this->belongsTo(Barang::class, 'barang_id', 'id');
    }
}
