<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'barangs';
    protected $fillable = [
        'nama',
        'slug',
        'kd_stok',
        'deskripsi',
        'harga',
        'stok',
    ];
    public function detailPesanan(){
        return $this->hasOne(DetailPesanan::class);
    }
}
