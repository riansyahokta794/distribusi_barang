<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    use HasFactory;
    protected $table = 'pesanans';
    protected $fillable = [
        'user_id',
        'no_pesanan',
        'tanggal',
        'status',
    ];
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function detailPesanan(){
        return $this->hasMany(DetailPesanan::class);
    }
    public function pengiriman(){
        return $this->hasOne(Pengiriman::class);
    }
}
