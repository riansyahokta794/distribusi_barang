<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detail_pesanans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pesanan_id')->constrained(table: 'pesanans', indexName: 'detail_pesanans_pesanan_id_foreign')->onDelete('cascade');
            $table->foreignId('barang_id')->constrained(table: 'barangs', indexName: 'detail_pesanans_barang_id_foreign')->onDelete('cascade');
            $table->integer('jumlah');
            $table->integer('harga');
            $table->integer('subtotal');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detail_pesanans');
    }
};
