<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengirimans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('no_pesanan')->constrained(table: 'pesanans', indexName: 'pengirimans_pesanan_id_foreign')->onDelete('cascade');
            $table->foreignId('transportasi_id')->constrained(table: 'transportasis', indexName: 'pengirimans_transportasi_id_foreign')->onDelete('cascade');
            $table->foreignId('user_id')->constrained(table: 'users', indexName: 'pengirimans_user_id_foreign')->onDelete('cascade');
            $table->date('tanggal');
            $table->string('alamat');
            $table->enum('status', ['Sedang Dikirim', 'Diterima'])->default('Sedang Dikirim');
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengirimans');
    }
};
