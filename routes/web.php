<?php

use App\Http\Controllers\Purchase\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Auth::routes();


Route::middleware('auth')->group(function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/produk', [App\Http\Controllers\PesananController::class, 'showProduk'])->name('show.produk');
    Route::get('/produk/{slug}/pesan', [App\Http\Controllers\PesananController::class, 'addPesanan'])->name('tambah.pesanan');
    Route::put('/produk/{id}/pesan', [App\Http\Controllers\PesananController::class, 'storePesanan'])->name('simpan.pesanan');


    Route::get('/pesanan', [App\Http\Controllers\PesananController::class, 'showPesanan'])->name('show.pesanan');
    Route::get('/pesanan/{id}/detail', [App\Http\Controllers\PesananController::class, 'showDetailPesanan'])->name('detail.pesanan');
    Route::get('/pesanan/{id}/ubah', [App\Http\Controllers\PesananController::class, 'editPesanan'])->name('edit.pesanan');
    Route::put('/pesanan/{id}/ubah', [App\Http\Controllers\PesananController::class, 'updatePesanan'])->name('update.pesanan');
    Route::get('/pesanan/{id}/proses', [App\Http\Controllers\PesananController::class, 'prosesPesanan'])->name('kirim.permintaan');
    Route::delete('/pesanan/hapus/{id}', [App\Http\Controllers\PesananController::class, 'destroy'])->name('hapus.pesanan');
    // Route::get('prosespesanan')

});

Route::prefix('purchase')->middleware(['auth', 'purchases'])->group(function(){
    Route::get('/', [DashboardController::class, 'index']);
});

Route::prefix('admin')->middleware(['auth','admin'])->group(function () {
    // Route::get('/', [App\Http\Controllers\Admin\DashboardController::class, 'index']);
    Route::get('/', [App\Http\Controllers\Admin\DashboardController::class, 'index']);
    Route::get('pelanggan', [App\Http\Controllers\Admin\PelangganController::class, 'index'])->name('pelanggan.show');
    Route::get('pelanggan/tambah', [App\Http\Controllers\Admin\PelangganController::class, 'create'])->name('pelanggan.tambah');
    Route::post('pelanggan/tambah', [App\Http\Controllers\Admin\PelangganController::class, 'store'])->name('pelanggan.simpan');
    Route::get('pelanggan/edit/{id}', [App\Http\Controllers\Admin\PelangganController::class, 'edit'])->name('pelanggan.edit');
    Route::put('pelanggan/edit/{id}', [App\Http\Controllers\Admin\PelangganController::class, 'update'])->name('pelanggan.update');
    Route::delete('pelanggan/delete/{id}', [App\Http\Controllers\Admin\PelangganController::class, 'destroy'])->name('konsumen.hapus');

    Route::get('/pesanan', [App\Http\Controllers\Admin\PesananController::class, 'index'])->name('show.pesanan.pelanggan');
    Route::get('/pesanan/{id}', [App\Http\Controllers\Admin\PesananController::class, 'showDetailPesanan'])->name('show.detailpesanan.pelanggan');
    Route::get('/pesanan/{id}/proses', [App\Http\Controllers\Admin\PengirimanController::class, 'prosesPesanan'])->name('terima.pesanan.pelanggan');
    Route::get('/pesanan/{id}/kirim', [App\Http\Controllers\Admin\PengirimanController::class, 'create'])->name('tambah.pengiriman.pelanggan');
    Route::post('pesanan/kirim', [App\Http\Controllers\Admin\PengirimanController::class, 'store'])->name('simpan.pengiriman.pelanggan');

    Route::get('pengiriman', [App\Http\Controllers\Admin\PengirimanController::class, 'index'])->name('show.pengiriman');
    Route::get('pengiriman/{id}', [App\Http\Controllers\Admin\PengirimanController::class, 'showDetail'])->name('show.detailpengiriman.pelanggan');
    

    Route::get('transportasi', [App\Http\Controllers\Admin\TransportasiController::class, 'index'])->name('show.transportasi');
    Route::get('transportasi/tambah', [App\Http\Controllers\Admin\TransportasiController::class, 'create'])->name('tambah.transportasi');
    Route::post('transportasi/tambah', [App\Http\Controllers\Admin\TransportasiController::class, 'store'])->name('simpan.transportasi');
    Route::get('transportasi/{id}/edit', [App\Http\Controllers\Admin\TransportasiController::class, 'edit'])->name('edit.transportasi');
    Route::put('transportasi/{id}/edit', [App\Http\Controllers\Admin\TransportasiController::class, 'update'])->name('update.transportasi');
    Route::delete('transportasi/{id}/hapus', [App\Http\Controllers\Admin\TransportasiController::class, 'destroy'])->name('hapus.transportasi');

    Route::get('barang', [App\Http\Controllers\Admin\BarangController::class, 'index'])->name('barang.show');
    Route::get('barang/tambah', [App\Http\Controllers\Admin\BarangController::class, 'create'])->name('barang.tambah');
    Route::post('barang/tambah', [App\Http\Controllers\Admin\BarangController::class, 'store'])->name('barang.simpan');
    Route::get('barang/edit/{id}', [App\Http\Controllers\Admin\BarangController::class, 'edit'])->name('barang.edit');
    Route::put('barang/edit/{id}', [App\Http\Controllers\Admin\BarangController::class, 'update'])->name('barang.update');
    Route::delete('barang/edit/{id}', [App\Http\Controllers\Admin\BarangController::class, 'destroy'])->name('barang.hapus');

    Route::get('user', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('user.show');
    Route::get('user/tambah', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('user.tambah');
    Route::post('user/tambah', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('user.simpan');
    Route::get('user/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('user.edit');
    Route::put('user/edit{id}', [App\Http\Controllers\Admin\UserController::class, 'update'])->name('user.update');
    Route::delete('user/hapus/{id}', [App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('user.hapus');


    Route::get('supplier', [App\Http\Controllers\Admin\SupplierController::class, 'index'])->name('supplier.show');
    Route::get('supplier/tambah', [App\Http\Controllers\Admin\SupplierController::class, 'create'])->name('supplier.tambah');
    Route::post('supplier/tambah', [App\Http\Controllers\Admin\SupplierController::class, 'store'])->name('supplier.simpan');
    Route::get('supplier/edit/{id}', [App\Http\Controllers\Admin\SupplierController::class, 'edit'])->name('supplier.edit');
    Route::put('supplier/edit/{id}', [App\Http\Controllers\Admin\SupplierController::class, 'update'])->name('supplier.update');
    Route::delete('supplier/delete/{id}', [App\Http\Controllers\Admin\SupplierController::class, 'destroy'])->name('supplier.hapus');
    // Route::get('kategori/tambah', [App\Http\Controllers\Admin\KategoriController::class, 'create']);
    // Route::post('kategori/tambah', [App\Http\Controllers\Admin\KategoriController::class, 'store']);
    // Route::get('kategori/edit/{kategori}', [App\Http\Controllers\Admin\KategoriController::class, 'edit']);
    // Route::put('kategori/edit/{kategori_id}', [App\Http\Controllers\Admin\KategoriController::class, 'update']);
    // Route::get('kategori/hapus/{kategori_id}', [App\Http\Controllers\Admin\KategoriController::class, 'destroy']);
});